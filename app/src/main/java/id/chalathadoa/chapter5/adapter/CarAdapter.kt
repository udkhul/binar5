package id.chalathadoa.chapter5.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import id.chalathadoa.chapter5.R
import id.chalathadoa.chapter5.databinding.ItemCarBinding
import id.chalathadoa.chapter5.model.getAllCarsItem

class CarAdapter: RecyclerView.Adapter<CarAdapter.CarViewHolder>() {

    val diffCallBack = object : DiffUtil.ItemCallback<getAllCarsItem>(){
        override fun areItemsTheSame(oldItem: getAllCarsItem, newItem: getAllCarsItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: getAllCarsItem, newItem: getAllCarsItem): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallBack)
    fun updateData(cars: List<getAllCarsItem>) = listDiffer.submitList(cars)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        //karena kita menggunakan binding, maka kita menginisialisaikan dgn binding
        val binding = ItemCarBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        //dan kita tidak memerlukan kode dibawah ini lagi
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_car, parent, false)
        return CarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    /**
     * untuk view binding, binding.root == view
     * jadi kita bisa ganti view dgn binding.root
     */
    inner class CarViewHolder(private val binding: ItemCarBinding) : RecyclerView.ViewHolder(binding.root){
        //jadi kita tidak perlu deklarasi seperti dibawah ini.

//        private val ivcar = view.findViewById<ImageView>(R.id.iv_car)
//        private val tvCarName : TextView = view.findViewById(R.id.tv_car_name)

        fun bind(item: getAllCarsItem){
            binding.apply {
                tvCarName.text = item.name
                //memunculkan gambar dgn glide karena url
                //agar lebih ringan maka kita menggunakan kode yg lebih panjang.
                val bgOptions = RequestOptions()
                    .placeholder(R.drawable.ic_add)
                Glide.with(itemView.context)
                    .load(item.image)
                    .apply(bgOptions)
                    .centerCrop()
                    .into(object : CustomTarget<Drawable?>() {
                        override fun onResourceReady(
                            resource: Drawable,
                            transition: Transition<in Drawable?>?
                        ) {
                            ivCar.setImageDrawable(resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            TODO("Not yet implemented")
                        }
                    })
            }
        }
    }
}