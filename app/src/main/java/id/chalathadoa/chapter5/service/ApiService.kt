package id.chalathadoa.chapter5.service

import id.chalathadoa.chapter5.model.RegisterRequest
import id.chalathadoa.chapter5.model.getAllCarsItem
import id.chalathadoa.chapter5.model.RegisterResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    //GET untuk memanggil semua data yg ada di server
    @GET("/admin/car")
    fun getAllCar(): Call<List<getAllCarsItem>>

    @POST("/admin/auth/register")
    //untuk mendapatkan request kita bisa menggunakan @Body
    fun registerAdmin(@Body request: RegisterRequest) : Call<RegisterResponse>
}