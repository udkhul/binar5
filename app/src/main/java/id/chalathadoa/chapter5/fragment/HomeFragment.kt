package id.chalathadoa.chapter5.fragment

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import id.chalathadoa.chapter5.R
import id.chalathadoa.chapter5.adapter.CarAdapter
import id.chalathadoa.chapter5.databinding.FragmentHomeBinding
import id.chalathadoa.chapter5.databinding.LayoutDialogAddBinding
import id.chalathadoa.chapter5.model.RegisterRequest
import id.chalathadoa.chapter5.model.RegisterResponse
import id.chalathadoa.chapter5.model.getAllCarsItem
import id.chalathadoa.chapter5.service.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var carAdapter: CarAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        getDataFromNetwork()
        buttonAdd()
    }

    private fun initRecyclerView(){
        carAdapter = CarAdapter()
        binding.rvCar.apply {
            adapter = carAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun getDataFromNetwork(){
        val apiService = ApiClient.instance
        apiService.getAllCar().enqueue(object : Callback<List<getAllCarsItem>> {
            override fun onResponse(
                call: Call<List<getAllCarsItem>>,
                response: Response<List<getAllCarsItem>>
            ) {
                //response.isSuccessful bisa juga ditulis response.code() == 200
                //isSuccessfull nya dari httpnya berhasil atau tdak, bukan dari kode api nya
                if (response.isSuccessful){
                    //setelah itu kita ngecek dari bodynya
                    if (!response.body().isNullOrEmpty()) {
                        response.body()?.let { carAdapter.updateData(it) }
                    }
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<List<getAllCarsItem>>, t: Throwable) {
                binding.pbCar.isVisible = false
            }
        })
    }
    private fun buttonAdd(){
        binding.fabAdd.setOnClickListener {
            createCustomDialog { email, password ->
                binding.pbCar.isVisible = true
                registerNewAdmin(email, password)
            }
        }
    }
    private fun createCustomDialog(onClickListener: (email: String, password: String) -> Unit){
        val binding = LayoutDialogAddBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        val view = binding.root
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setView(view)

        val dialog = dialogBuilder.create()
        binding.apply {
            btnRegis.setOnClickListener {
                onClickListener.invoke(etEmail.text.toString(), etPassword.text.toString())
            }
        }
        dialog.show()
    }

    private fun registerNewAdmin(email: String, password: String){
        val apiService = ApiClient.instance
        val request = RegisterRequest(email = email, password = password, role = "admin")
        apiService.registerAdmin(request).enqueue(object: Callback<RegisterResponse> {
            override fun onResponse(
                call: Call<RegisterResponse>,
                response: Response<RegisterResponse>
            ) {
                if (response.isSuccessful) {
                    Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                } else {
                    //kita akan menggunakan message yg tersedia pada body, bukan langsung pada toast.
                    val objError = JSONObject(response.errorBody()!!.string())
                    val message = objError.getString("message")
                    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                binding.pbCar.isVisible = false
            }

        })
    }
}