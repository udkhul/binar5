package id.chalathadoa.chapter5

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TryViewModel: ViewModel() {
    val nilaiBaru = MutableLiveData(0)

    fun increment(){
        //untuk viewModel wajib menggunakan .value atau .postvalue
        nilaiBaru.value = nilaiBaru.value?.plus(1)
    }
    fun decrement(){
        nilaiBaru.value = nilaiBaru.value?.minus(1)
    }
}