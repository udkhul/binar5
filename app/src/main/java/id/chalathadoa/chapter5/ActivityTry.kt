package id.chalathadoa.chapter5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import id.chalathadoa.chapter5.databinding.ActivityTryBinding

class ActivityTry : AppCompatActivity() {
    private lateinit var binding: ActivityTryBinding

    private var mCounter: Int = 0 //membuat variabel untuk menampung angka

    //membuat variabel untuk viewmodel. Tidak bisa langsung namaClass()
    //harus ada by viewmodels() nya
    private val viewModeltry: TryViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //withoutViewModel()
        withViewModel()
    }

    /**
     * TANPA VIEW MODEL
     */

    private fun withoutViewModel(){
        binding.btnPlus.setOnClickListener {
            mIncrementCount()
        }
        binding.btnMinus.setOnClickListener {
            mDecrementCount()
        }
    }

    private fun mIncrementCount(){
        val newText = binding.tvCount.text.toString().toInt().plus(1)
        //mCounter += 1
        updateTv(newText)
    }
    private fun mDecrementCount(){
        val newText = binding.tvCount.text.toString().toInt().minus(1)
        updateTv(newText)
    }

    /**
     * DENGAN VIEW MODEL
     */

    private fun withViewModel(){
        binding.btnPlus.setOnClickListener {
            viewModeltry.increment()
        }
        binding.btnMinus.setOnClickListener {
            viewModeltry.decrement()
        }

        //menampilkan nilai baru ke textview
        viewModeltry.nilaiBaru.observe(this){
            updateTv(it)
        }
    }

    //membuat fungsi untuk menampilkan angka ke text view
    private fun updateTv(newValue: Int){
        binding.tvCount.text = newValue.toString()
    }
}